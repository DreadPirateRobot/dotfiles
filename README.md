# .files
Hello! These are my personal config files. These are what I spend too much of my time tweaking, but I'm happy with where a lot of them are. 

Besides what you can find in the files, the software I use and that I would endorse include:
 - neovim (dotfile available)
 - nnn (no dotfile nessesary, I am using mostly stock with quitcd included in my zsh dotfile)
 - zsh (dotfile available)
 - alacritty (dotfile available)
 - Sway wm (dotfile available)
 - Arch Linux
 - Rust and python as my main languages, both very good
 - Firefox ESR, good ol' firefox without a lot of the mozilla BS

## Values
 - The basics: speed, security, functionality
 - I don't care too much about privacy, but to an extent. I still want control over what I send but I have nothing to hide so I'm not a fanatic. Determine your personal threat level.
 - Maintainability, sustainability
 - Balance of stability and progressiveness (which I still haven't settled on)

## Todo
 - better color scheme for Sway
 - broken alias in zsh

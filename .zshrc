

#
#### ~/.zshrc
#


# Lines configured by zsh-newuser-install
HISTFILE=~/.bash_history
HISTSIZE=1000
SAVEHIST=30000
setopt autocd beep extendedglob nomatch
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/toast/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

## environment variables
export EDITOR=nvim
export VISUAL=nvim
export PAGER=less

# Add cargo to path
export PATH=$PATH:~/.cargo/bin/

# If running from tty1 start sway immediately
if [ "$(tty)" = "/dev/tty1" ]; then
    exec sway
fi

if [ -f /usr/share/nnn/quitcd/quitcd.sh ]; then
    source /usr/share/nnn/quitcd/quitcd.sh
fi

function zle-line-init zle-keymap-select {
    PROMPT="[%B%F{green}%n@%m%f%b] %B%F{blue}%1~%f%b ${${KEYMAP/vicmd/>}/(main|viins)/%#} "
    # Kinda proud of that :D it is mode sensitive, so insert and normal mode can be
    # differentiated
    
    zle reset-prompt
}

zle -N zle-line-init
zle -N zle-keymap-select
 
### ALIASAE 



alias yarr='cd ~/Music && youtube-dl -f 140' 
alias youtube-dl='youtube-dl -f bestvideo[ext=mp4]+bestaudio[ext=m4a]/mp4'
# Haha XD copyright infringement is fun and cool actually
# With zsh make sure you put the youtube URL in quotes (if you know a fix I
# don't have to that would be really welcome)

alias q='exit'
alias aliases='nvim ~/.zshrc && source ~/.zshrc'
# Quick debugging of zsh

alias bright='cd /sys/class/backlight/'
alias gay='figlet -t "Bi gang rise up!" | lolcat -F 0.5' 
# Be gay do crime

alias vi='nvim'
alias fuck='sudo $(fc -ln -1)'
# sudo !! but it always works

# alias neofetch='neofetch | lolcat -F 0.5'
alias ls='ls --color=auto'
alias cmatrix='cmatrix -ab -u1' # | lolcat -F 0.0003

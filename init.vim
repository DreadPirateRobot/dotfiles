set linebreak	
set incsearch
set hlsearch
tnoremap <Esc> <C-\><C-n>
inoremap fd <Esc>
noremap <Space> :set rnu! rnu? nu! nu?<cr><cr>

"nothing too special here. fd as escape alternative, escape exits a terminal, spacebar toggles line numbers. 
